import datetime
import json
import tempfile
import os
import requests
import subprocess


def __get_actual_time__() -> str:
    now = datetime.datetime.now()

    return \
        str(now.year) +\
        f'{now.month:02}' +\
        f'{now.day:02}' + "-" +\
        f'{now.hour:02}' + "h" +\
        f'{now.minute:02}' + "m" +\
        f'{now.second:02}' + "s"


def get_repos_based_on_pagination(page_loop, repository_address):
    data = requests.get(repository_address + "?page=" + str(page_loop))
    jsonData = json.loads(data.text)

    temporary_file_resource = open(temporary_list_repos, 'w')

    for jsonUnit in jsonData:
        temporary_file_resource.write(jsonUnit["git_url"] + "\n")

    temporary_file_resource.close()


def get_temporary_list_repos_address():

    current_time = __get_actual_time__()

    if os.name == 'nt':
        temporary_path_folder = tempfile.gettempdir()
        return os.path.join(temporary_path_folder, current_time)
    else:
        return os.sep + os.path.join('tmp', current_time)


def ask_local_path_to_download():
    local_path_to_download = input("Please, provides the local path for download: ")
    if not os.path.isdir(local_path_to_download):
        print("The provided folder path does not exists.")
        ask_local_path_to_download()
    else:
        return local_path_to_download


def pagination_is_not_empty(temporary_list_repos):

    if not os.path.isdir(temporary_list_repos):
        return True

    file_resource = open(temporary_list_repos, "r")
    file_lines = file_resource.readlines()

    if len(file_lines) == 1 and file_lines[0] == "":
        return False

    return True


def extract_folder_name_from_repository_path(repository):
    name_parts = repository.split("/")
    git_project_key = name_parts[4]
    git_project_key_parts = git_project_key.split(".")
    folder_name = git_project_key_parts[0]
    return folder_name


def download_or_pull_repo(repository):
    repo_folder = extract_folder_name_from_repository_path(repository)

    FNULL = open(os.devnull, 'w')
    
    if os.path.isdir(repo_folder):
        print("Repository " + repo_folder)
        os.chdir(repo_folder)

        try:
            subprocess.check_call(['git', 'rev-parse', '--is-inside-work-tree'], stdout=FNULL)
            result_is_git_controlled = True
        except:
            result_is_git_controlled = False

        if result_is_git_controlled:
            print("Updating...")
            subprocess.call(['git', 'pull'], stdout=FNULL)
        else:
            print("Cloning...")
            subprocess.call(['git', 'clone', repository, '.'], stdout=FNULL)
    else:
        os.makedirs(repo_folder)
        os.chdir(repo_folder)
        subprocess.call(['git', 'clone', repository, '.'])

    os.chdir('..')


def loop_through_temporary_file_and_download(temporary_list_repos, page_loop):
    file_resource = open(temporary_list_repos, "r")
    file_lines = file_resource.readlines()

    for repository in file_lines:
        download_or_pull_repo(repository)

    page_loop += 1

    return page_loop


def download_repos_from_page(page_loop, temporary_list_repos, repository_address):
    while pagination_is_not_empty(temporary_list_repos):
        get_repos_based_on_pagination(page_loop, repository_address)
        page_loop = loop_through_temporary_file_and_download(temporary_list_repos, page_loop)


temporary_list_repos = get_temporary_list_repos_address()
page_loop = 1
github_user_alias = input("Please, provides the github user alias: ")
repository_address = "https://api.github.com/users/" + github_user_alias + "/repos"
local_path_to_download = ask_local_path_to_download()
os.chdir(local_path_to_download)
download_repos_from_page(page_loop, temporary_list_repos, repository_address)

