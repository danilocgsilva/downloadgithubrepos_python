from install_assets.files import files
from install_assets.files import os_entry
import os
import shutil
import stat


for file in files:

    src = file["source"]
    dst = file["destiny"]

    shutil.copy(src, dst)


os.chmod(os_entry, stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH | stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH)
