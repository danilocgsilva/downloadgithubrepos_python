import os

def __get_os_execution_path__():
    if os.name == 'nt':
        return os.getenv('WINDIR')
    else:
        return os.sep + os.path.join('usr', 'local', 'bin')


def __get_destiny_os_entry_name__():
    if os.name == 'nt':
        return 'dgithubrepos.cmd'
    else:
        return 'dgithubrepos'


def __get_os_source_entry__():
    if os.name == 'nt':
        return 'windows_dgithubrepos'
    else:
        return 'unix_dgithubrepos'

os_execution_path = __get_os_execution_path__()

os_entry_destination_name = __get_destiny_os_entry_name__()

os_entry = os.path.join(os_execution_path, os_entry_destination_name)

os_source_entry = __get_os_source_entry__()

files = [
    {
        'source': os.path.join('.', '__main__.py'),
        'destiny': os.path.join(os_execution_path, 'dgithubrepos.py')
    },
    {
        'source': os.path.join('install_assets', os_source_entry),
        'destiny': os_entry
    }
]
